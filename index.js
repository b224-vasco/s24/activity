const getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);

const address = ["258 Washington Ave NW","California","90011"];
const [street, state, zipcode] = address;


console.log(`I live at ${street}, ${state} ${zipcode}`)

const animal = {
	animalName: "Lolong",
	animalSpecies: "saltwater Crocodile",
	weight: 1075,
	feet: 20,
	inches: 3
};

function fullAnimal(animalName, animalSpecies, weight, feet, inches){
	console.log(`${animalName} was a ${animalSpecies}. He weighed ${weight} kgs with a measurement of ${feet} ${inches}`)
};

fullAnimal(animal.animalName, animal.animalSpecies, animal.weight, animal.feet, animal.inches);

const numbers=[1,2,3,4,5,15];

numbers.forEach((number)=>{
	console.log(`${number}`);
});

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const myDog = new Dog();
console.log(myDog);

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Daschund";

console.log(myDog);